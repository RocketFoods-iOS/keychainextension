//
//  Credentials.swift
//  KeychainExtension
//
//  Created by Nikita Arutyunov on 15.10.2019.
//  Copyright © 2019 Nikita Arutyunov. All rights reserved.
//

import Alamofire

public struct Credentials: Codable {
    public let accessToken: String
    public let refreshToken: String
    
    /// Expires timestamp
    public var expires: TimeInterval
    
    /// Expires in seconds
    public let expiresIn: TimeInterval

    public var isExpired: Bool {
        let requestTimeOffset = TimeInterval(-5) // FIXME: Ask Alex for offset
        
        return Date() > Date(timeIntervalSince1970: expires + requestTimeOffset)
    }
    
    public var authorizationHeader: HTTPHeaders {
        return ["Authorization": "Bearer \(accessToken)"]
    }
    
    public init(accessToken: String, refreshToken: String, expires: TimeInterval, expiresIn: TimeInterval) {
        self.accessToken = accessToken
        self.refreshToken = refreshToken
        self.expires = expires
        self.expiresIn = expiresIn
    }
}
