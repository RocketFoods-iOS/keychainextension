//
//  KeychainExtension.swift
//  KeychainExtension
//
//  Created by Nikita Arutyunov on 15.10.2019.
//  Copyright © 2019 Nikita Arutyunov. All rights reserved.
//

import KeychainAccess
import Security
import RFExtensions

extension Keychain: InjectableService { }

public extension Keychain { // FIXME: Throws
    class var getQuery: CFDictionary {
        [
            kSecClass as String: kSecClassGenericPassword,
            kSecAttrService as String: "RFAuthService",
            kSecReturnData as String: true,
        ] as CFDictionary
    }
    
    class func addQuery(_ data: Data) -> CFDictionary {
        [
            kSecClass as String: kSecClassGenericPassword,
            kSecAttrService as String: "RFAuthService",
            kSecValueData as String: data,
        ] as CFDictionary
    }
    
    class var updateQuery: CFDictionary {
        [
            kSecClass as String: kSecClassGenericPassword,
            kSecAttrService as String: "RFAuthService",
        ] as CFDictionary
    }
    
    class func updateAttributes(_ data: Data) -> CFDictionary {
        [
            kSecAttrService as String: "RFAuthService",
            kSecValueData as String: data,
        ] as CFDictionary
    }
    
    class var removeQuery: CFDictionary {
        [
            kSecClass as String: kSecClassGenericPassword,
            kSecAttrService as String: "RFAuthService",
        ] as CFDictionary
    }
    
    var credentials: Credentials? {
        get {
            var item: CFTypeRef?
            
            let status = SecItemCopyMatching(Self.getQuery, &item)
            
            guard status == errSecSuccess,
                let credentialsData = item as? Data,
                let credentials = try? JSONDecoder().decode(Credentials.self, from: credentialsData)
                else { return nil }
            
            return credentials
        }
        
        set {
            guard let newValue = newValue else {
                let deleteStatus = SecItemDelete(Self.removeQuery)
                
                guard deleteStatus == errSecSuccess else { return } // FIXME: throw
                
                return
            }
            
            let credentials = Credentials(
                accessToken: newValue.accessToken,
                refreshToken: newValue.refreshToken,
                expires: Date().addingTimeInterval(newValue.expiresIn).timeIntervalSince1970,
                expiresIn: newValue.expiresIn
            )
            
            guard let key = try? JSONEncoder().encode(credentials) else { return }
            
            _ = SecItemDelete(Self.removeQuery)
            
            let addStatus = SecItemAdd(Self.addQuery(key), nil)
            
            guard addStatus == errSecSuccess else { return } // FIXME: throw
        }
    }
}
