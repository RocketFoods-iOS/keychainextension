.PHONY: all carthage

carthage:
	carthage update --use-xcframeworks --new-resolver --no-use-binaries --no-build; \
	carthage build --use-xcframeworks --no-skip-current --cache-builds;

all: carthage
